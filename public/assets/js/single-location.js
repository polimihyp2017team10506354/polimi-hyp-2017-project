
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


$(document).ready(function() {

   $.get("/locations/"+getUrlParameter('id'), function(data, status) {
      console.log( JSON.parse(data))
      var location = JSON.parse(data)


         console.log(location.location[0]);
   $("#name").append(
   `     <img src="/assets/img/locations/${location.location[0].id}.jpg" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 img-responsive img-top-pagine-descrizione" >
            <h3 class="titolo-img-top">${location.location[0].name}</h3>

   `
   );

   $("#description").append(

   `            <h2 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Descrizione</h2>
                <p class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">${location.location[0].description}</p>
                <h4 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Servizio Offerti</h4>




   `
   );


   for (var i = 0 ; i < location.services.length ; i++ ) {
      $("#description").append(
               `
               <a href= "singolo-servizio.html?areaId=${location.location[0].id}&serviceId=${location.services[i].id}">
                <div class=" col-sm-12 col-md-6 col-lg-4 col-xl-4  justify-content-end content-img box-bottom">
                      <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                          <img src="/assets/img/services/${location.services[i].id}.jpg" class="col-12 col-sm-12  col-md-12 col-lg-12 col-xl-12" >
                      </div>
                    <p class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">${location.services[i].name}</p>
                </div>
               </a>
         `

      );
   }


$("#description").append(

               `<div class="box-bottom-single-location">
                     <p><span>Tel.</span>${location.location[0].phone}</p>
                     <p><span>Mail.</span>${location.location[0].email}</p>
                     <p><span>Indirizzo.</span>${location.location[0].address}</p>
               </div>

   `
   );

}


    );



});
