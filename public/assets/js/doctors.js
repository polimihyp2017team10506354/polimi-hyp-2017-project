
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


$(document).ready(function() {
  var doctors
  var datas
  var locations
  $("#locationList").change(function(){
    console.log($("#locationList").val())
    var value = $("#locationList").val();
    location.href = window.location.pathname + "?location="+value;
  });
  $.get("/locations", function(data, status){
    locations=JSON.parse(data);
    for (var i=0; i< locations.length; i++){
      if(locations[i].id == getUrlParameter('location')){
        $("#locationList").append(`<option value="${locations[i].id}" selected="selected">${locations[i].name}</option>`);
      }else {
        $("#locationList").append(`<option value="${locations[i].id}">${locations[i].name}</option>`);
      }


    }
  });
  if(typeof getUrlParameter('location') === 'undefined' || getUrlParameter('location') === 'default'){
    $.get("/doctors", function(data, status){
        doctors = JSON.parse(data);
        console.log(doctors);
        for (var i=0; i< doctors.length; i++){
          $("#doctorsList").append(
              `                        <a href= "scheda-dottore.html?id=${doctors[i].id}">
                <div class=" col-sm-12 col-md-4 col-lg-3 col-xl-4  justify-content-end content-img box-bottom position-doctors">
                      <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                          <img src="/assets/img/doctors/${doctors[i].id}.jpg" class="col-12 col-sm-12  col-md-12 col-lg-12 col-xl-12" >
                      </div>
                    <p class="doctors-name">${doctors[i].name}</p>
                </div>
               </a>`
          );
        }
    });
  }else {
    $.get("/doctors?location=" + getUrlParameter('location'), datas ,function(data, status){
        doctors = JSON.parse(data);
        console.log(doctors);
        for (var i=0; i< doctors.length; i++){
          $("#doctorsList").append(
            `                        <a href= "scheda-dottore.html?id=${doctors[i].id}">
                <div class=" col-sm-12 col-md-6 col-lg-3 col-xl-4  justify-content-end content-img box-bottom position-doctors">
                      <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                          <img src="/assets/img/doctors/${doctors[i].id}.jpg" class="col-12 col-sm-12  col-md-12 col-lg-12 col-xl-12">
                      </div>
                    <p class="doctors-name">${doctors[i].name}</p>
                </div>
               </a>`
          );
        }
    });
  }




});
