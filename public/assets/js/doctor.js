

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


$(document).ready(function() {

   $.get("/doctors/"+getUrlParameter('id'), function(data, status) {
      console.log( JSON.parse(data))
      var doctorPage = JSON.parse(data)


   $("#name").append(

   `       <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 contenitore-img-top">
            <img src="/assets/img/doctors/${doctorPage.doctor[0].id}.jpg" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 img-responsive img-top-pagine-descrizione" >
            <h3 class="titolo-img-top">${doctorPage.doctor[0].name}</h3>
        </div>

   `
   );




   $("#description").append(

   `             <h2 class="col-sm-12 col-md-12 col-lg-12 col-xl-12">Curriculum</h2>
                <p class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                        ${doctorPage.doctor[0].cv}
                      
                </p>
                <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4" id = "column">
                </div>

      `
   );

      if(doctorPage.responsibleOfArea.length >0){
   var responsibleOfArea = doctorPage.responsibleOfArea[0]

    $("#column").append(

   `  

                    
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 cnt-skil-dottore">
                        <h4>Respondabile reparto</h4>
                        <a href= "tipo-servizio.html?id=${responsibleOfArea.id}">
                        <img src="/assets/img/areas/${responsibleOfArea.id}.jpg">
                        </a>
                        <p>${responsibleOfArea.name}</p>
                    </div>

                    </div> 

    `
   );
};

 if(doctorPage.responsibleOfService.length >0){
   var responsibleOfService = doctorPage.responsibleOfService[0]

    $("#column").append(

   `  
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 cnt-skil-dottore">
                        <h4>Responsabile di servizio</h4>
                         <a href= "singolo-servizio.html?areaId=${responsibleOfService.areasId}&serviceId=${responsibleOfService.id}">
                        <img src="/assets/img/services/${responsibleOfService.id}.jpg">
                         </a>
                        <p>${responsibleOfService.name}</p>
                    </div>

    `
   );
};

 if(doctorPage.operatingIn.length >0){
   var operatingIn = doctorPage.operatingIn[0]
    $("#column").append(

   `  
                   
                  
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 cnt-skil-dottore">
                        <h4>Servizi offerti</h4>
                        <a href= "singolo-servizio.html?areaId=${operatingIn.areasId}&serviceId=${operatingIn.id}">
                        <img src="/assets/img/services/${operatingIn.id}.jpg">
                        </a>
                        <p>${operatingIn.name}</p>
                    </div>
                

    `
   );
};




}


    );
});
