var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


$(document).ready(function() {


      $.get("/areas/"+getUrlParameter('areaId'), function(data, status) {
      console.log( JSON.parse(data))
      var area = JSON.parse(data)
      var idArea = getUrlParameter('areaId')
      console.log("AREA : " + idArea)


for (var i = 0 ; i < area.services.length ; i++ ) {
   var idService = area.services[i].id
   console.log("singolo-servizio.html?areaId=${area.singArea.id}&serviceId=${area.services[i].id}")
      $("#menu").append(

               `
                       <a href= "singolo-servizio.html?areaId=${area.singArea.id}&serviceId=${area.services[i].id}">   <li>${area.services[i].name}</li>  </a>
                     

         `

           );
   
}}
      );
   

   

   $.get("/areas/"+getUrlParameter('areaId') + "/services/" +getUrlParameter('serviceId') , function(data, status) {
      console.log( JSON.parse(data))
      var servicePage = JSON.parse(data)


   $("#name").append(
   `
            <img src="/assets/img/services/${servicePage.service[0].id}.jpg" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 img-responsive img-top-pagine-descrizione" >
            <h3 class="titolo-img-top">${servicePage.service[0].name}</h3>

   `
   );


$("#description").append(

   `            <h2 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Descrizione</h2>
                <p class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                   ${servicePage.service[0].description}
                </p>



                <div class=" col-sm-12 col-md-6 col-lg-4 col-xl-4 box">
                    <h4 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Dove</h4>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-end content-img "> 
                         <a href= "location-singola.html?id=${servicePage.location[0].id}">
                       <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                          <img src="/assets/img/locations/${servicePage.location[0].id}.jpg" class="" >
                        </div>
                        </a>
                        <p>${servicePage.location[0].name}</p>
                    </div>
                </div>

                 <div class=" col-sm-12 col-md-6 col-lg-4 col-xl-4 box">
                    <h4 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Responsabile</h4>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-end content-img">
                      <a href= "scheda-dottore.html?id=${servicePage.responsible.id}">
                        <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <img src="/assets/img/doctors/${servicePage.responsible.id}.jpg" class="" >

                        </div>
                     </a>
                        <p>${servicePage.responsible.name}</p>
                    </div>
                </div>

                <div class=" col-sm-12 col-md-6 col-lg-4 col-xl-4 box">
                    <h4 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Reparto</h4>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 justify-content-end content-img">
                       <a href= "tipo-servizio.html?id=${servicePage.service[0].areasId}">
                       <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                           <img src="/assets/img/areas/${servicePage.service[0].areasId}.jpg" class="" >
                       </div>
                       </a>
                        <p>${servicePage.area[0].name}</p>
                    </div>
                </div>

               <div class=" col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id = "doctors">
                    <h4 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">I nostri dottori</h4>
 

                     
                </div>     




   `
   );




   for (var i = 0 ; i < servicePage.doctors.length ; i++ ) {
      $("#doctors").append(
               `
               <a href= "scheda-dottore.html?id=${servicePage.doctors[i].id}">
                <div class=" col-sm-12 col-md-6 col-lg-4 col-xl-4  justify-content-end content-img box-bottom">
                      <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                          <img src="/assets/img/doctors/${servicePage.doctors[i].id}.jpg" class="col-12 col-sm-12  col-md-12 col-lg-12 col-xl-12" >
                      </div>
                    <p class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">${servicePage.doctors[i].name}</p>
                </div>
               </a>
         `

      );
   }

 



}


    );


 
});
