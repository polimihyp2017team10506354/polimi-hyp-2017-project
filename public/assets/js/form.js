$(document).ready(function() {
    $.get("/services", function(data, status) {
        console.log(JSON.parse(data))
        var services = JSON.parse(data)
        for (var i = 0; i < services.length; i++) {
            $("#services").append(`<option value="${services[i].sid}">${services[i].sname}</option>`);
        }
    });
});


function isFormFilled(formData) {
    let x = {};
    for (var pair of formData.entries()) {
        if (pair[1] == null || pair[1] == "") {
            return false;
        }
    }
    return true;
}

function formDataAsJSON(formData) {
    let x = {};
    for (var pair of formData.entries()) {
        x[pair[0]] = pair[1];
    }
    return JSON.stringify(x);
}

function clickSubmitReservationData() {
    let headers = new Headers();
    headers.set("Content-Type", "application/json");

    var formData = new FormData(document.getElementById("reservationForm"));

    if (isFormFilled(formData)) {
        let formdataJSON = formDataAsJSON(formData);
        fetch("/reservation", {
                method: "POST",
                body: formdataJSON,
                headers: headers
            })
            .then(response => { console.log(response); return response;}) // log response
            .then(response => {
                   $("#reservationForm")[0].reset();

                $(".alert").remove();
                if (response.status == "200") {
                    $("#reservationForm").append(`<div class="alert alert-success" role="alert">Prenotazione inserita con successo , verrai ricontattato a breve.</div>`);
                } else {
                    $("#reservationForm").append(`<div class="alert alert-danger" role="alert">Oooops!! , si è verificato un problema , riprova più tardi.</div>`);
                }
            });
    } else {
        $(".alert").remove();
        $("#reservationForm").append(`<div class="alert alert-danger" role="alert">Oooops!! Completa tutto il form</div>`);
    }
}
