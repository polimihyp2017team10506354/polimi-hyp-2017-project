
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


$(document).ready(function() {

   $.get("/areas/"+getUrlParameter('id'), function(data, status) {
      console.log( JSON.parse(data))
      var area = JSON.parse(data)


   $("#name").append(

   `     <img src="/assets/img/areas/${area.singArea.id}.jpg" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 img-responsive img-top-pagine-descrizione" >
            <h3 class="titolo-img-top">${area.singArea.name}</h3>

   `
   );

   $("#description").append(

   `            <h2 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Descrizione</h2>
                <p class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">${area.singArea.description}</p>
                <h4 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Servizio Offerti</h4>




   `
   );


   for (var i = 0 ; i < area.services.length ; i++ ) {
      $("#description").append(
               `
               <a href= "singolo-servizio.html?areaId=${area.singArea.id}&serviceId=${area.services[i].id}">
                <div class=" col-sm-12 col-md-6 col-lg-4 col-xl-4  justify-content-end content-img box-bottom">
                      <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                          <img src="/assets/img/services/${area.services[i].id}.jpg" class="col-12 col-sm-12  col-md-12 col-lg-12 col-xl-12" >
                      </div>
                    <p class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">${area.services[i].name}</p>
                </div>
               </a>
         `

      );
   }


$("#description").append(

               `<div class="box-bottom">
                               <h4 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">Responsabile</h4>
                           <a href= "scheda-dottore.html?id=${area.responsible.id}">
                           <div class=" col-sm-12 col-md-4 col-lg-4 col-xl-4  justify-content-end content-img">
                            <div class="cnt-img col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                   <img src="/assets/img/doctors/${area.responsible.id}.jpg" class="" >
                           </div>
                                <p>${area.responsible.name}</p>
                            </div>
                           </a>

               </div>

   `
   );

}


    );



});
