const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const sqlDbFactory = require("knex");
const process = require("process");
const _ = require("lodash");

var path = require("path");
//let sqlDb;
let serverPort = process.env.PORT || 5000;
//Dichiarazione dati da caricare
let locationsList = require("./other/locationsdata.json");
let areasList = require("./other/areasdata.json");
let servicesList = require("./other/servicesdata.json");
let doctorsList = require("./other/doctorsdata.json");


function initSqlDB() {
 /* Locally we should launch the app with TEST=true to use SQLlite:
    > TEST=true node ./index.js */

  if(process.env.TEST) {
    sqlDb = sqlDbFactory({
      client: "sqlite3",
      debug: true,
      connection: {
        filename: "./other/clinicdb.sqlite"
      }
    });
  }else{
    sqlDb = sqlDbFactory({
      debug: true,
      client: "pg",
      connection: process.env.DATABASE_URL,
      ssl: true
        });
      }
}

function initDb() {
  //controlla se esiste la tabella locations e, se non esiste la crea e mappa i dottori dal json
  sqlDb.schema.hasTable("locations").then(exists => {
    if (!exists) {
      sqlDb.schema
      .createTable("locations", table => {
        table.increments();
        table.string("name"); //max 255
        table.bigInteger("phone");
        table.string("email");
        table.string("address");
        table.text("description");
      })
      .then(() => {
        return Promise.all(
          _.map(locationsList, l =>{
            delete l.id;
            return sqlDb("locations").insert(l);
          })
        );
      });
    }
  });
  //controlla se esiste la tabella areas e, se non esiste la crea e mappa i dottori dal json
  sqlDb.schema.hasTable("areas").then(exists => {
    if (!exists) {
      sqlDb.schema
      .createTable("areas", table => {
        table.increments();
        table.string("name");
        table.bigInteger("phone");
        table.string("email");
        table.text("description");
        table.integer("locationId").references("id").inTable("locations").notNullable();
      })
      .then(() => {
        return Promise.all(
          _.map(areasList, a =>{
            delete a.id;
            return sqlDb("areas").insert(a);
          })
        );
      });
    }
  });
  //controlla se esiste la tabella services e, se non esiste la crea e mappa i dottori dal json
  sqlDb.schema.hasTable("services").then(exists => {
      if (!exists) {
        sqlDb.schema
        .createTable("services", table => {
          table.increments();
          table.string("name");
          table.text("description");
          table.integer("areasId").references("id").inTable("areas").notNullable();
        })
        .then(() => {
          return Promise.all(
            _.map(servicesList, s =>{
              delete s.id;
              return sqlDb("services").insert(s);
            })
          );
        });
      }
    });
  //controlla se esiste la tabella doctors e, se non esiste la crea e mappa i dottori dal json
  sqlDb.schema.hasTable("doctors").then(exists => {
    if (!exists) {
      sqlDb.schema
      .createTable("doctors", table => {
        table.increments();
        table.string("name");
        table.text("cv");
        table.integer("operatingInId").references("id").inTable("services").notNullable();
        table.integer("respServiceId").references("id").inTable("services");
        table.integer("respAreaId").references("id").inTable("areas");
      })
      .then(() => {
        return Promise.all(
          _.map(doctorsList, d =>{
            delete d.id;
            return sqlDb("doctors").insert(d);
          })
        );
      });
    }
  });
  //controlla se esiste la tabella reservation e, se non esiste la crea
  sqlDb.schema.hasTable("reservation").then(exists => {
    if (!exists) {
      sqlDb.schema
      .createTable("reservation", table => {
        table.increments();
        table.string("name");
        table.string("surname");
        table.boolean("gender");
        table.string("email");
        table.bigInteger("phone");
        table.text("description");
        table.integer("serviceId").references("id").inTable("services").notNullable();
      })
      .then(() => { });
    }
  });

}
app.use(express.static(path.join(__dirname, './public')));
app.use('/', express.static(path.join(__dirname, './public/pages')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// /* Register REST entry point */

//Tutte le location in ordine crescente per nome
app.get("/locations",function(req, res){
  let myQuery = sqlDb("locations");
  myQuery = myQuery.orderBy("name", "asc");
  myQuery.then(result => {
    res.send(JSON.stringify(result));
  });
});

//Singola locations
app.get("/locations/:id",function(req, res){
  let idl = parseInt(req.params.id);
  //estraggo la location
  sqlDb('locations').where('id', idl).then(location => {
    //estraggo tutti i servizi che quella location ospita
    sqlDb('services').select('services.id','services.name').innerJoin('areas', 'services.areasId', 'areas.id').where('areas.locationId', idl).then(services =>{
      result = {
        location,
        "services":services
      };
      res.send(JSON.stringify(result));
    })
  });
});

//Service by location di tutte le location
app.get("/services",function(req, res){
    //estraggo tutti i servizi che quella location ospita
    sqlDb('services').select('locations.id as lid','locations.name as lname', 'locations.phone', 'locations.email', 'locations.address', 'services.id as sid','services.name as sname', 'areas.id as aid').innerJoin('areas', 'services.areasId', 'areas.id').innerJoin('locations', 'areas.locationId', 'locations.id').then(services =>{
      res.send(JSON.stringify(services));
    });
});
//Tutti i dottori in ordine alfabetico e con filtro su location
app.get("/doctors",function(req, res){
  let idl = req.query.location;
  let myQuery = sqlDb("doctors");
  if(typeof req.query.location != 'undefined'){
    myQuery = myQuery.innerJoin('services','doctors.operatingInId','services.id').innerJoin('areas', 'services.areasId', 'areas.id').where('areas.locationId', idl);
  }

  myQuery = myQuery.select('doctors.id', 'doctors.name').orderBy("doctors.name", "asc");
  myQuery.then(result => {
    res.send(JSON.stringify(result));
  });
});

//Singolo dottore
app.get("/doctors/:id",function(req, res){
  let idd = parseInt(req.params.id);
  //seleziona lo specifico dottore
  sqlDb('doctors').where('id', idd).then(doctor =>{
    //seleziona il servizio che fornisce
    sqlDb('services').select('services.id','services.name', 'services.areasId').innerJoin('doctors','services.id','doctors.operatingInId').where('doctors.id', idd).then(operating =>{
      //seleziona il servizio di cui è responsabile se ne esiste uno
      sqlDb('services').select('id','name', 'areasId').where('id',doctor[0].respServiceId).then(sResposinble => {
        //seleziona l'area di cui è responsabile se ne esiste una, concatena tutto e invia
        sqlDb('areas').select('id','name').where('id',doctor[0].respAreaId).then(aResposinble => {
          result = {
            doctor,
            'operatingIn':operating,
            'responsibleOfService':sResposinble,
            'responsibleOfArea':aResposinble
          };
          res.send(JSON.stringify(result));
        });
      });
    });
  });
});

//Tutte le aree in ordine crescente per nome
app.get("/areas",function(req, res){
  let myQuery = sqlDb("areas");
  myQuery = myQuery.orderBy("name", "asc");
  myQuery.then(result => {
    res.send(JSON.stringify(result));
  });
});

//Area specifica con i servizi appartenenti alla stessa e il responsabile
app.get("/areas/:id",function(req, res){
  let ida = parseInt(req.params.id);
  //seleziona l'area specifica
  sqlDb('areas').where('id', ida).then(area =>{
    sqlDb('services').select('id','name').where('areasId', ida).then(services =>{
      sqlDb('doctors').select('id','name').where('respAreaId', ida).then(responsible =>{
        var singArea = area[0];
        var singResponsible = responsible[0];
        result = {
          singArea,
          'responsible':singResponsible,
          'services': services
        };
        res.send(JSON.stringify(result));
      })

    });

  });
});

//Singolo servizio
app.get("/areas/:ida/services/:ids",function(req, res){
  let ida = parseInt(req.params.ida);
  let ids = parseInt(req.params.ids);
  let responsible = -1;
  let i = 0;

  sqlDb('services').where('id',ids).then(service =>{
    //seleziona i dottori che ci lavorano e il responsabile
    sqlDb('doctors').select('id', 'name').where('operatingInId',ids).orWhere('respServiceId',ids).then(doctors =>{
      for(i = 0; i < doctors.length; i++){
        if(doctors[i].respServiceId == service[0].responsible ){
          responsible = i;
        }
      }
      //seleziona l'aerea a cui appartiene
      sqlDb('areas').select('name','locationId').where('id', ida).then(area =>{
        //seleziona dove si trova
        sqlDb('locations').select('id', 'name').where('id', area[0].locationId).then(location => {
          result = {
            service,
            'responsible': doctors[responsible],
            doctors,
            area,
            location
          };
          res.send(JSON.stringify(result));
        });
      });

    });
  });

});

//inserimento prenotazione
app.post("/reservation", function(req, res) {
  let toappend = {
    name: req.body.name,
    surname: req.body.surname,
    gender: req.body.gender,
    email: req.body.email,
    phone: req.body.phone,
    description: req.body.description,
    serviceId: req.body.serviceId
  };
  console.log(toappend.name);
  sqlDb("reservation").insert(toappend).then(ids => {
    let id = ids[0];
    res.send(_.merge({ id, toappend }));
  });
});


app.set("port", serverPort);

initSqlDB();
initDb();

/* Start the server on port 5000 */
app.listen(serverPort, function() {
  console.log(`Your app is ready at port ${serverPort}`);

});
