# GENERAL INFORMATION
* Heroku URL: https://polimi-hyp-2017-team-10506354.herokuapp.com
* Bitbucket repo URL:https://bitbucket.org/polimihyp2017team10506354/polimi-hyp-2017-project
* Team administrator : Simone, Biffi, 10506354, polimi-hyp-2017-10506354
* Team member n°2 : Ilyas, Inajjar, 10424129, polimi-hyp-2017-10424129

# API DOC

## Location

**Tutte le location**

Ritorna tutte le location ordinate per nome

```/locations [GET]```

*Risposta*:
```
{"id":int,
"name":string,
"phone":int,
"email":string,
"address":string,
"description":string
}
```
**Singola location**

Ritorna tutte la location selezionata

```/locations/:id [GET]```

| Name          | Type          | Description                   |
| :-----------: |:-------------:| :---------------------------: |
| id            | integer       | id della location selezionata |

*Risposta*:
```
{"location":
    {"id": int,
    "name": string,
    "phone": int,
    "email": string,
    "address": string,
    "description": string
    },
    "services":
        {"id": int,
        "name": string
        }
}
```

## Aree

**Tutte le aree**

Ritorna tutte le aree ordinate per nome

```/areas [GET]```

*Risposta*:
```
{"id":int,
"name": string,
"phone": int,
"email": string,
"description": string,
"locationId": int
}
```
**Singola area**

Ritorna l'area selezionata con il nome del responsabile e i servizi all'interno dell'area

```/areas/:id [GET]```

| Name          | Type          | Description                   |
| :-----------: |:-------------:| :---------------------------: |
| id            | integer       | id dell'area selezionata      |

*Risposta*:
```
{
"area":
    {"id":int,
    "name": string,
    "phone": int,
    "email": string,
    "description": string,
    "locationId": int},
    "responsible":
        {"id":int,
        "name": string
        },
    "services":
        {"id": int,
        "name": string
        }
}
```

## Servizi

**Singolo servizio**

Ritorna il servizio selezionato con il nome del responsabile, l'area di appartenenza e la location in cui viene offerto

```/areas/:ida/services/:ids [GET]```

| Name          | Type          | Description                                    |
| :-----------: |:-------------:| :---------------------------------------------:|
| ida           | integer       | id dell'area di appartenenza del servizio      |
| ids           | integer       | id del servizio selezionato                    |

*Risposta*:
```
{"service":
    {"id": int,
    "name": string,
    "description": string,
    "areasId": int
    },
    "responsible":
        {"id": int,
        "name": string
        },
    "doctors":
        {"id": int,
        "name": string},
    "area":
        {"name": string,
        "locationId": int},
    "location":
        {"id": int,
        "name": string
        }
}
```
**Tutti i servizi per location**

Ritorna tutti i servizi separati per location ordinati per id della location

```/services [GET]```

*Risposta*:
```
{"address": string,
"lid": int,
"lname": string,
"email": string,
"phone": int,
"sid": int,
"sname": string,
"aid": int
}
```

## Dottori

**Tutti i dottori con filtri**

Ritorna tutti i dottori ordinati per nome, o nel caso in cui venga inviato il parametro 'location' ritorna i dottori ordinati per nome che soddisfano il filtro

```/doctors?location=lid [GET]```

| Name          | Type          | Description                                    |
| :-----------: |:-------------:| :---------------------------------------------:|
| lid      | integer / null| id della location su cui filtrare    |

*Risposta*:
```
{"id": int,
"name": string
}
```

**Singolo dottore**

Ritorna tutti il dottore selezionato

```/doctors/:id [GET]```

| Name          | Type          | Description                                    |
| :-----------: |:-------------:| :---------------------------------------------:|
| :id      | integer | id del dottore selezionato      |

*Risposta*:
```
{"doctor":
    {"id": int,
    "name": string,
    "cv": text,
    "operatingInId": int
    },
    "operatingIn":
        {"id": int,
        "name": string,
        "areasId": int
        },
    "responsibleOfService":
        {"id": int,
        "name": string,
        "areasId": int
        },
    "responsibleOfArea":
        {"id": int,
        "name": string
        }
}
```

## Prenotazione

**Inserisci una prenotazione**

Inserisce una nuova prenotazione, passare i parametri come body della request

```/reservation [POST]```

| Name          | Type          | Description                   |
| :-----------: |:-------------:| :---------------------------------------------:|
| name      | string| il nome di chi effettua la prenotazione      |
| surname      | string| il cognome di chi effettua la prenotazione      |
| gender      | boolean| il sesso di chi effettua la prenotazione, 1 maschio, 0 femmina      |
| email      | string| l'email di chi effettua la prenotazione      |
| phone      | integer| il numero di telefono di chi effettua la prenotazione      |
| description      | text| descrizione aggiuntiva, se necessaria      |
| serviceId      | integer| l'id del servizio prenotato      |


*Risposta*:
```
{"id": int,
"toappend":
    {"name": string,
    "surname": string,
    "gender": boolean,
    "email": string,
    "phone": int,
    "description": string,
    "serviceId": int
    }
}
```
